---
title: "Pontos Turísticos"
date: 2021-09-18T23:28:40-03:00
draft: false
---

# Pontos turísticos favoritos

{{<line_break>}}

Abaixo, listarei meus locais favoritos em SP, espero que gostem!

{{<line_break>}}

## Parque Ibirapuera
Parque mais famoso da cidade, é uma área para famílias e ótima para fazer um piquenique, relaxar, praticar esportes, fazer exercícios e apreciar arte. Ele fica cheio nos fins de semana e é um destino de lazer popular entre os moradores. O parque sedia vários eventos ao longo do ano, como shows musicais gratuitos e um espetáculo de águas e luz em comemoração às festividades do fim do ano. Há ainda dois monumentos importantes para você visitar: o Obelisco e o Monumento às Bandeiras. 

{{<line_break>}}

## MASP
O MASP, Museu diverso, inclusivo e plural, tem a missão de estabelecer, de maneira crítica e criativa, diálogos entre passado e presente, culturas e territórios, a partir das artes visuais. Para tanto, deve ampliar, preservar, pesquisar e difundir seu acervo, bem como promover o encontro entre públicos e arte por meio de experiências transformadoras e acolhedoras.

{{<line_break>}}

## Bairro da Liberdade
O bairro da Liberdade não pode faltar no seu roteiro em São Paulo, são tantas atrações pra aproveitar e se sentir no Japão, que até mesmo quem mora na cidade pode ficar na dúvida de o que fazer no bairro. Mas se engana quem pensa que o bairro da Liberdade conta apenas com influência japonesa, a cultura oriental marca presença com os imigrantes chineses, coreanos, tailandeses e tawaineses, além disso a cultura afro faz parte da história do bairro. 

{{<line_break>}}

## Jardim Botânico
Jardim Botânico, a melhor opção de passeio para crianças e famílias conhecerem mais sobre plantas e ficarem em contato com a natureza. O melhor Jardim Botânico está localizado em São Paulo e têm papel fundamental na conservação de espécies, na realização de pesquisa científica, no desenvolvimento sustentável e na realização de práticas educativas que permitem a construção de novos conhecimentos. Um passeio imperdível para o despertar de valores, emoções e conexão de crianças e famílias com a natureza.
